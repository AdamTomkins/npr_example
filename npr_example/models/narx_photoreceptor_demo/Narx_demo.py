#import sys
#sys.path.insert(0,'/home/ra/Documents/Work/npr_example')
import pdb
import argparse
import itertools

from neurokernel.core_gpu import Module       

import numpy as np

import networkx as nx
import h5py

import matplotlib.pyplot as plt
import cPickle
import scipy
from scipy import io

from neurokernel.tools.logging import setup_logger
from neurokernel.core_gpu import Manager
import npr_example
import npr_example.LPU
from npr_example.LPU.LPU_vis import LPU
import neurokernel.mpi_relaunch

data_dir = 'data/'
data = scipy.io.loadmat(data_dir + 'u.mat')
u_data = np.ravel(data['u_hat_pr1'])[-4000:]
u_data = u_data.astype(np.float64)

y_data = np.ravel(data['y_output'])[-4000:]
y_data = y_data.astype(np.float64)

dt = 1./400
dur = dt* len(y_data) - 7
Nt = len(y_data) - 7

with h5py.File(data_dir + 'phr_inputs.h5', 'w') as f:
    f.create_dataset('array', (Nt, 1),
                     dtype=np.double,
                     #data=u_data[7:])
                     data=u_data[6:-1])

G = nx.DiGraph() # or nx.MultiDiGraph()
G.add_nodes_from([0])

u_toggle = 1
y_toggle = 1 
G.node[0] = {
    'model': 'Narx_pr',
    'name': 'Narmax Test',
    'extern': True,      # indicates whether the neuron can receive an external input signal
    'public': False,      # indicates whether the neuron can emit output to other LPUs 
    'spiking': False,     # indicates whether the neuron outputs spikes or a membrane voltage
    'selector': '/a[0]', # every public neuron must have a selector 
    'V':  float(y_data[1]+68)*y_toggle,
    'Y_1':float(y_data[2]+68)*y_toggle,
    'Y_2':float(y_data[3]+68)*y_toggle,
    'Y_3':float(y_data[4]+68)*y_toggle,
    'Y_4':float(y_data[5]+68)*y_toggle,
    'Y_5':float(y_data[6]+68)*y_toggle,
    'Y_6':float(y_data[7]+68)*y_toggle,
    'U_1':float(u_data[0])*u_toggle,
    'U_2':float(u_data[1])*u_toggle,
    'U_3':float(u_data[2])*u_toggle,
    'U_4':float(u_data[3])*u_toggle,
    'U_5':float(u_data[4])*u_toggle,
    'U_6':float(u_data[5])*u_toggle,
    'U_7':float(u_data[6])*u_toggle,
    'Cnt_Y':0,
    'Cnt_U':0
} 

nx.write_gexf(G,'Narx_graph.gexf.gz')


N_neurons = G.number_of_nodes()

parser = argparse.ArgumentParser()
parser.add_argument('--visualise', default=False,
                    dest='visualise', action='store_true',
                    help='Enable visualisation')
parser.add_argument('--debug', default=False,
                    dest='debug', action='store_true',
                    help='Write connectivity structures and inter-LPU routed data in debug folder')
parser.add_argument('-l', '--log', default='none', type=str,
                    help='Log output to screen [file, screen, both, or none; default:none]')
parser.add_argument('-s', '--steps', default=Nt, type=int,
                    help='Number of steps [default: %s]' % Nt)
parser.add_argument('-g', '--gpu_dev', default=0, type=int,
                    help='GPU device number [default: 0]')
args = parser.parse_args()


file_name = None

screen = False
if args.log.lower() in ['file', 'both']:
    file_name = 'narx.log'
if args.log.lower() in ['screen', 'both']:
    screen = True
logger = setup_logger(file_name=file_name, screen=screen)


man = Manager()
(n_dict, s_dict) = LPU.lpu_parser('Narx_graph.gexf.gz')
print n_dict.keys()
print s_dict.keys()

man.add(LPU, 'Narx', dt, n_dict, s_dict, 
        input_file=data_dir +'phr_inputs.h5',
        output_file=data_dir +'Narx_output.h5',
        device=0, debug=args.debug,visualise=args.visualise)

man.spawn()
man.start(steps=Nt)
man.wait()

