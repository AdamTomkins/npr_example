#!/usr/bin/env python

"""
Create a photoreceptor LPU and given the input and buffere stimulus.
"""

import pdb
from itertools import product
import sys

import numpy as np
import h5py
import networkx as nx

import cPickle as pickle

def create_lpu(output_file_name, lpu_name, N_sensory, params_file_name):
    """
    Create a parallel photoreceptor LPU.

    Parameters
    ----------
    output_file_name : str
        Output GEXF file name.
    lpu_name : str
        Name of the LPU to be generated
    N_sensory : int
        Number of sensory neurons.
    data_file_name : str
        Filename of the input file with neuron parameters.
    """
    #########################################
    # We need to have a valid input file first
    #########################################
    # pdb.set_trace()    
    # Set numbers of neurons:
    neu_num = N_sensory

    # Neuron ids are between 0 and the total number of neurons:
    G = nx.DiGraph()
    G.add_nodes_from(range(neu_num))

    f_in = h5py.File(params_file_name) 
    u_data = f_in['u']
    #y_data = f_in['y']
    

    idx = 0
    for i in range(neu_num):
        name = lpu_name+"_"+str(i)
           
        # All local neurons are graded potential only:
        G.node[idx] = {
            'model': 'Narx_pr',
            'selector': '/a['+str(i)+']', # every public neuron must have a selector
   
            #'V': float( y_data[i,0]+68),
            #'Y_1':float(y_data[i,1]+68),
            #'Y_2':float(y_data[i,2]+68),
            #'Y_3':float(y_data[i,3]+68),
            #'Y_4':float(y_data[i,4]+68),
            #'Y_5':float(y_data[i,5]+68),
            #'Y_6':float(y_data[i,6]+68),
            'V':  float(0),
            'Y_1':float(0),
            'Y_2':float(0),
            'Y_3':float(0),
            'Y_4':float(0),
            'Y_5':float(0),
            'Y_6':float(0),
            'U_1':float(u_data[0,i]),
            'U_2':float(u_data[1,i]),
            'U_3':float(u_data[2,i]),
            'U_4':float(u_data[3,i]),
            'U_5':float(u_data[4,i]),
            'U_6':float(u_data[5,i]),
            'U_7':float(u_data[6,i]),
            'Cnt_Y':0,
            'Cnt_U':0,
            'spiking': False,
            'name': name+'_g',
            'extern': True,
            'public': True
        }

                    
        idx += 1

    nx.write_gexf(G, output_file_name)

def create_input_p(input_file_name, N_sensory, N_steps, output_file_name):
    """
    Create input stimulus for sensory neurons in artificial LPU from a matlab file

    Creates an HDF5 file containing input signals for the specified number of
    neurons. The signals consist of a rectangular pulse of specified duration
    and magnitude.

    Parameters
    ----------
    input_file_name : str
        Name of input mat file to extract data from
        Should contain an array of u data
    N_sensory : int
        Number of sensory neurons.
    N_steps : int
        The number of timesteps extracted from the input_file
    output_file_name : str
        The name of the file that will be saved with _data and _parameters extensions
 
   Ensures
   ----------- 
    Creation of two files
    file_name_data
        > To be fed into the Manager as input
    file_name_parameters
        > To be fed into the create_lpu function to supply model lag-parameters
    """
    #data = sio.loadmat('u.mat')
    #u = data['u_input']
    data = pickle.load(open(input_file_name,'rb'))
    lag = 7

    I = np.zeros((N_steps, N_sensory), dtype=np.double)
    params =  np.zeros((lag,N_sensory), dtype =np.double)
    

    for i in range(0, N_sensory):
        I[:,i] =  data['e'+str(i)]['clean_pulses'][lag:N_steps+lag]
        params[:,i] =  data['e'+str(i)]['clean_pulses'][0:lag]

    # rescale hack
    I[I>2] = 0.053
    I[I==1.5] = 0.03
    params[params>2] = 0.053
    params[params==1.5] = 0.03
    

    with h5py.File(output_file_name+"_pars.h5", 'w') as f:
        f.create_dataset('u', (lag, N_sensory),
                         dtype=np.float64,
                         data=params)
    print params

    # Output file with simulation data

    with h5py.File(output_file_name+"_data.h5", 'w') as f:
        f.create_dataset('array', (N_steps, N_sensory),
                         dtype=np.float64,
                         data=I)


def create_input(input_file_name, N_sensory, N_steps, output_file_name):
    """
    Create input stimulus for sensory neurons in artificial LPU.

    Creates an HDF5 file containing input signals for the specified number of
    neurons. The signals consist of a rectangular pulse of specified duration
    and magnitude.

    Parameters
    ----------
    input_file_name : str
        Name of input HDF5 file to extract data from
        Should contain an array of u data.
    N_sensory : int
        Number of sensory neurons.
    N_steps : int
        The number of timesteps extracted from the input_file
    output_file_name : str
        The name of the file that will be saved with _data and _parameters extensions
 
   Ensures
   ----------- 
    Creation of two files
    file_name_data
        > To be fed into the Manager as input
    file_name_parameters
        > To be fed into the create_lpu function to supply model lag-parameters
    """
    
    lag = 7 
    # Open read in the input_file_name
    f_in = h5py.File(input_file_name) 
    print input_file_name
    print f_in
    pdb.set_trace()
    u = f_in["array"]
    #y = f_in['y']
    # Extract first 7 values of u
    u_params = u[:N_sensory,:lag]
    print u_params    
    # Extract first 7 values of y
    #y_params = y[:N_sensory,:7]
    # Output parameter values file    
    with h5py.File(output_file_name+"_pars.h5", 'w') as f:
        f.create_dataset('u', (lag, N_sensory),
                         dtype=np.float64,
                         data=u_params)
        #f.create_dataset('y', (7, N_sensory),
        #                 dtype=np.float64,
        #                 data=y_params)
    # Extract first 7 values of u
    u_data = u[:N_sensory,lag:N_steps+lag]
    # iExtract first 7 values of y
    #y_data = y[:N_sensory,7:]

    # Output file with simulation data
    
    with h5py.File(output_file_name+"_data.h5", 'w') as f:
        f.create_dataset('array', (N_steps, N_sensory),
                         dtype=np.float64,
                         data=u_data)
        #f.create_dataset('y', (N_steps-7, N_sensory),
        #                 dtype=np.float64,
        #                 data=y_data)




if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    #parser.add_argument('lpu_file_name', nargs='?', default='narx_pr_lpu.gexf.gz',
    #                    help='LPU file name')
    parser.add_argument('in_file_name', nargs='?', default='narx_pr_input.h5',
                        help='Input file name')
    parser.add_argument('neu_num', nargs='?', default='1',
                        help='Input file name')
    parser.add_argument('-s', type=int,
                        help='Seed random number generator')
    parser.add_argument('-l', '--lpu', type=str, default='gen',
                        help='LPU name')

    args = parser.parse_args()

    if args.s is not None:
        np.random.seed(args.s)

    neu_num = int(args.neu_num)
    print 'Simulating ' + str(neu_num) + ' neurons.'
    steps = 1000
    #create_input(args.in_file_name, neu_num, steps, 'narx_pr')
    #create_lpu(args.lpu_file_name, narx_pr_10, neu_num)
    create_input_p(args.in_file_name, neu_num, steps, 'narx_pr')
    create_lpu("narx_pr_network.gexf.gz", "narx_pr_10", neu_num,"narx_pr_pars.h5")

