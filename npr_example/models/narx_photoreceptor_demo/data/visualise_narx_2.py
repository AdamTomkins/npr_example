import pdb
import h5py
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt
"""
f = h5py.File('Narx_V.h5')
V = f['array']
f = h5py.File('Narx_I.h5')
I = f['array']
f = h5py.File('phr_inputs.h5')
Input = f['array']

f = h5py.File('Narx_Y_1.h5')
Y_1 = f['array']
f = h5py.File('Narx_Y_2.h5')
Y_2 = f['array']
f = h5py.File('Narx_Y_3.h5')
Y_3 = f['array']
f = h5py.File('Narx_Y_4.h5')
Y_4 = f['array']
f = h5py.File('Narx_Y_5.h5')
Y_5 = f['array']
f = h5py.File('Narx_Y_6.h5')
Y_6 = f['array']


f = h5py.File('Narx_U_1.h5')
U_1 = f['array']
f = h5py.File('Narx_U_2.h5')
U_2 = f['array']
f = h5py.File('Narx_U_3.h5')
U_3 = f['array']
f = h5py.File('Narx_U_4.h5')
U_4 = f['array']
f = h5py.File('Narx_U_5.h5')
U_5 = f['array']
f = h5py.File('Narx_U_6.h5')
U_6 = f['array']

f = h5py.File('Narx_Cnt_Y.h5')
Cnt_Y = f['array']

f = h5py.File('Narx_Cnt_U.h5')
Cnt_U = f['array']
"""
f = h5py.File('data/Narx_output_gpot.h5')
gpot = f['array']
pdb.set_trace()
plt.plot(gpot)
plt.show()
#f = h5py.File('Narx_synapses.h5')
#synapses = f['array']

# Also Current, Synapses ect
## Basic Graph
plt.figure('test V')
plt.subplot(2, 2, 1) 
print V[:20]
plt.plot(V)
plt.xlabel('V')
plt.subplot(2, 2, 2)
plt.plot(I)
plt.xlabel('I')
plt.subplot(2, 2, 3)
plt.plot(gpot)
plt.xlabel('gplot')
plt.subplot(2, 2, 4)
plt.plot(synapses)
plt.xlabel('synapses')

plt.show()

## graph of YS
#plt.subplot(3, 2, 1) 
#plt.plot(Y_1)
#plt.subplot(3, 2, 2)
#plt.plot(Y_2)
#plt.subplot(3, 2, 3)
#plt.plot(Y_3)
#plt.subplot(3, 2, 4)
#plt.plot(Y_4)
#plt.subplot(3, 2, 5)
#plt.plot(Y_5)
#plt.show()

#plt.subplot(3, 2, 1) 
#plt.plot(U_1)
#plt.subplot(3, 2, 2)
#plt.plot(U_2)
#plt.subplot(3, 2, 3)
#plt.plot(U_3)
#plt.subplot(3, 2, 4)
#plt.plot(U_4)
#plt.subplot(3, 2, 5)
#plt.plot(U_5)
#plt.subplot(3, 2, 6)
#plt.plot(U_6)
#plt.show()

#plt.subplot(2, 1, 1) 
#plt.plot(Cnt_Y)
#plt.subplot(2, 1, 2)
#plt.plot(Cnt_U)
#plt.show()

plt.plot(Y_1[0:20],label='Y_1')
plt.hold(True)
plt.plot(Y_2[0:20],label='Y_2')
plt.plot(Y_3[0:20],label='Y_3')
plt.plot(Y_4[0:20],label='Y_4')
plt.plot(Y_5[0:20],label='Y_5')
plt.plot(Y_6[0:20],label='Y_6')
plt.legend()
plt.show()

import scipy 
from scipy import io

data = scipy.io.loadmat('u.mat')
just = scipy.io.loadmat('data/just_NARX_output.mat')
just_neuro = scipy.io.loadmat('just_narx_from_neuro_2.mat')
V2 = data['y_output'][-4000:-3000] + 68
V3 = data['y_model'][-4000+7:] 
#V3 = data['y_model'][-4000:] 
V4 = just['simout_just_NARX'][-4000+7:]
V5 = just_neuro['simout_just_NARX']

f = plt.figure('Narx_validation_neurokernel')
ax = f.add_subplot(111) 
ax.plot(V3)
ax.plot(V)
ax.plot(V5)
#ax.plot(V4)
print V.dtype
#ax.plot(V2)
ax.set_xlabel('samples')
ax.set_ylabel('voltage')
ax.legend(('PR_LMC','neurokernel','NARX'))
f.show()
plt.show()

f = plt.figure('Back2Backdiff')
ax = f.add_subplot(111) 
ax.plot(V-V5[:-2])
ax.set_xlabel('samples')
ax.set_ylabel('voltage')
f.show()
plt.show()


f = plt.figure('NArx_validation_inputs')
plt.plot(I,label='NK recieved input')
plt.plot(Input,label='Narx_pr_data')
plt.legend()
plt.show()


