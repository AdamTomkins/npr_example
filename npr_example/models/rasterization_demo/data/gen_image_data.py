import argparse
import sys
import itertools
import networkx as nx
import pdb
import argparse
import itertools
import numpy as np
import h5py
import matplotlib.pyplot as plt
import scipy
from scipy import io
import cv2
import pickle 

def main(argv=None):
    
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input_file', default='jeff.jpeg',type=str,help='The name of the image file to import')
    parser.add_argument('-o', '--output', default='image_demo_input',type=str,help='the name of the output array')
    parser.add_argument('-f', '--frames', default=2000,type=int,help='The number of frames to construct') 
    parser.add_argument('-x', '--xcut', default=100,type=int,help='The number of pixles in the x dir')
    parser.add_argument('-y', '--ycut', default=100,type=int,help='The number of pixles in the y dir') 
    parser.add_argument('-s', '--scale', default=0.2,type=int,help='scale the image to be between 0 and the number, default 0.2') 
    parser.add_argument('-m', '--mode', default="raster_single",type=str,help='the type of output to create. Accepts still,cycle,raster') 
    
    parser.add_argument('--inverse', default=0,type=int,help='invert the input') 
    parser.add_argument('--debug', default=False, dest='debug', action='store_true', help='Write connectivity structures and inter-LPU routed data in debug folder')

    args = parser.parse_args()
    
    file = args.input_file
    im = cv2.imread(file,cv2.CV_LOAD_IMAGE_GRAYSCALE).astype(np.float32)

    # scale the input signals between 0 and 0.2 or as passed in args.scale
    im  = ((1.0*im) / 256) * args.scale
    
    # invert the stimulus if required
    if args.inverse:
        im = -1.*im + 2*im.mean()
    
    params = {}
    params['output'] = args.output
    params['input'] = args.input_file
    params['inverse'] = args.inverse
    params['mode']   = args.mode


    pdb.set_trace()
    if args.mode == "still":
        still_image(im,args,params)
    elif args.mode == "cycle":
        cycle_image(im,args,params)
    elif args.mode == "raster_single":
        rasterize_single(im,args,params)
    elif args.mode == "raster_multi":
        rasterize_multi(im,args,params)
    else:
        print "Invalid input mode, aborting process."

def rasterize_single(im,args,params):
    # creates one neuron, and feeds in the input as a time series
    params['x'] = 1 
    params['y'] = 1
    params['frames'] = im.shape[1]*im.shape[0]
    params['total'] = 1
    pickle.dump( params, open( args.output+".p", "wb" ) )
    
    I = im.astype(np.double)
    with h5py.File(args.output+'.h5', 'w') as f:
        f.create_dataset('array', (params['frames'], params['total']),
                     dtype=np.double,
                     data=I)

    gen_network(params['total'],args.output,im[1,:])
    print "rasterised for a single neuron"

def rasterize_multi(im,args,params):
    pdb.set_trace()
    params['x'] = im.shape[0] 
    params['y'] = 1
    params['frames'] = im.shape[1]
    params['total'] = im.shape[0]
    pickle.dump( params, open( args.output+".p", "wb" ) )
    
    I = im.astype(np.double)
    with h5py.File(args.output+'.h5', 'w') as f:
        f.create_dataset('array', (params['frames'], params['total']),
                     dtype=np.double,
                     data=I)

    gen_network(params['total'],args.output,im[1,:])

    print "rasterised for %s neurons" % params['total']

def still_image(im,args,params):    

    params['x'] = im.shape[0] 
    params['y'] = im.shape[1]
    params['frames'] = args.frames
    params['total'] = im.shape[0]*im.shape[1]
    pickle.dump( params, open( args.output+".p", "wb" ) )
    
    im = im.ravel()
    I = np.tile(im,(args.frames,1))    

    with h5py.File(args.output+'.h5', 'w') as f:
        f.create_dataset('array', (params['frames'], params['total']),
                     dtype=np.double,
                     data=I)
    gen_network(params['total'],args.output,im.ravel())

def cycle_image(im,args,params):

    params['x'] = im.shape[0] 
    params['y'] = im.shape[1]
    params['frames'] = args.frames
    params['total'] = im.shape[0]*im.shape[1]
    pickle.dump( params, open( args.output+".p", "wb" ) )

    pdb.set_trace()
    I = np.zeros((params['frames'],params['total']))
    for i in range(args.frames):
        im_temp = im.ravel()
        I[i,:] = im_temp
        im = np.roll(im,1,axis=0);
            

    with h5py.File(args.output+'.h5', 'w') as f:
        f.create_dataset('array', (args.frames,params['total']),
                     dtype=np.double,
                     data=I)
    gen_network(params['total'],args.output,im.ravel())

def gen_network(neu_num,lpu_name,im):
    
    # Neuron ids are between 0 and the total number of neurons:
    G = nx.DiGraph()
    G.add_nodes_from(range(neu_num))

    idx = 0 
    for i in range(neu_num):
        name = lpu_name+"_"+str(i)
    
        # All local neurons are graded potential only:
        G.node[i] = { 
            'model': 'Narx_pr',
            'selector': '/a['+str(i)+']', # every public neuron must have a selector   
            'V':  float(0),
            'Y_1':float(0),
            'Y_2':float(0),
            'Y_3':float(0),
            'Y_4':float(0),
            'Y_5':float(0),
            'Y_6':float(0),
            'U_1':float(im[i]),
            'U_2':float(im[i]),
            'U_3':float(im[i]),
            'U_4':float(im[i]),
            'U_5':float(im[i]),
            'U_6':float(im[i]),
            'U_7':float(im[i]),
            'Cnt_Y':0,
            'Cnt_U':0,
            'spiking': False,
            'name': name+'_g',
            'extern': True,
            'public': True
        }

    nx.write_gexf(G, lpu_name + '.gexf.gz')
        
if __name__ == "__main__":

    sys.exit(main())
    

