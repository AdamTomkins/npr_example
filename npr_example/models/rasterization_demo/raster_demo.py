import pdb
import numpy as np
import argparse
import itertools

from neurokernel.core_gpu import Module       
import networkx as nx
import h5py
import matplotlib.pyplot as plt
import pickle
import scipy
from scipy import io


from neurokernel.tools.logging import setup_logger
from neurokernel.core_gpu import Manager

import narx_retina
import narx_retina.LPU
from narx_retina.LPU.LPU_vis import LPU

import neurokernel.mpi_relaunch

import matplotlib.cm as cm

parser = argparse.ArgumentParser()

parser.add_argument('--visualise', default=False,
                    dest='visualise', action='store_true',
                    help='Enable visualisation')
parser.add_argument('--debug', default=False,
                    dest='debug', action='store_true',
                    help='Write connectivity structures and inter-LPU routed data in debug folder')
parser.add_argument('-l', '--log', default='none', type=str,
                    help='Log output to screen [file, screen, both, or none; default:none]')
parser.add_argument('-n', '--network', default='image_demo_input.gexf.gz', type=str,
                    help='Network file')
parser.add_argument('-i', '--input_sig', default='image_demo_input', type=str,
                    help='Network file')
parser.add_argument('-o', '--output_sig', default='image_demo_output', type=str,
                    help='Network file')
parser.add_argument('-g', '--gpu_dev', default=0, type=int,
                    help='GPU device number [default: 0]')
args = parser.parse_args()
dt = 1./400           # time resolution of model execution in seconds
params = pickle.load( open( 'data/' + args.input_sig +".p", "rb" ) )
Nt = params['frames']
dur = Nt*dt


file_name = None

screen = False
if args.log.lower() in ['file', 'both']:
    file_name = 'narx.log'
if args.log.lower() in ['screen', 'both']:
    screen = True
logger = setup_logger(file_name=file_name, screen=screen)


N_neurons = params['total']




man = Manager()
(n_dict, s_dict) = LPU.lpu_parser('data/'+args.network)
print n_dict.keys()
print s_dict.keys()

man.add(LPU, 'Narx', dt, n_dict, s_dict, 
        input_file='data/'+args.input_sig+'.h5',
        output_file='data/'+args.output_sig+'.h5',
        device=0, debug=args.debug,visualise=args.visualise)

man.spawn()
man.start(steps=Nt)
man.wait()

f =h5py.File('data/image_demo_output_gpot.h5')
g =h5py.File('data/image_demo_input.h5')

f_plot_data = (np.array(f.values())).astype(np.float32)
input_data = (np.array(g.values())).astype(np.float32)

plot_data = (f_plot_data/np.max(f_plot_data))*256
plot_data = plot_data.astype(np.int)

f, (ax1, ax2) = plt.subplots(1, 2, sharey=True)
ax1.imshow(input_data[0,:,:].reshape(100,100), cmap = cm.Greys_r);
ax1.set_title('Input Data')
ax2.imshow(plot_data[0,:,:].reshape(100,100),  cmap = cm.Greys_r);
ax2.set_title('Gpot Data')
#plt.colorbar()
plt.show()



