#!usr/bin/env python

import numpy as np

import pycuda.gpuarray as garray
from pycuda.tools import dtype_to_ctype
import pycuda.driver as cuda
from pycuda.compiler import SourceModule

from neurokernel.LPU.utils.simpleio import *
from baseneuron import BaseNeuron

class Narx_pr(BaseNeuron):
    def __init__(self, n_dict, V, dt, debug=False,LPU_id=None):                     ## Keep V as the final membrane potential 
        self.num_neurons = len(n_dict['id'])
        self.dt = np.double(dt)                                         ## dt and steps are strange for Morris Lescar, edit for Narx.
        self.steps = 1
        self.debug = debug
        self.LPU_id = LPU_id
        self.V = garray.to_gpu(np.asarray(n_dict['V'], dtype=np.float64))
        self.V = V#garray.to_gpu(np.asarray(n_dict['V'], dtype=np.float64))

        self.Y_1 = garray.to_gpu(np.asarray(n_dict['Y_1'], dtype=np.float64))
        self.Y_2 = garray.to_gpu(np.asarray(n_dict['Y_2'], dtype=np.float64))
        self.Y_3 = garray.to_gpu(np.asarray(n_dict['Y_3'], dtype=np.float64))
        self.Y_4 = garray.to_gpu(np.asarray(n_dict['Y_4'], dtype=np.float64))
        self.Y_5 = garray.to_gpu(np.asarray(n_dict['Y_5'], dtype=np.float64))
        self.Y_6 = garray.to_gpu(np.asarray(n_dict['Y_6'], dtype=np.float64))
       
        self.U_1 = garray.to_gpu(np.asarray(n_dict['U_1'], dtype=np.float64))
        self.U_2 = garray.to_gpu(np.asarray(n_dict['U_2'], dtype=np.float64))
        self.U_3 = garray.to_gpu(np.asarray(n_dict['U_3'], dtype=np.float64))
        self.U_4 = garray.to_gpu(np.asarray(n_dict['U_4'], dtype=np.float64))
        self.U_5 = garray.to_gpu(np.asarray(n_dict['U_5'], dtype=np.float64))
        self.U_6 = garray.to_gpu(np.asarray(n_dict['U_6'], dtype=np.float64))
        self.U_7 = garray.to_gpu(np.asarray(n_dict['U_7'], dtype=np.float64))
        
        self.Cnt_Y = garray.to_gpu(np.asarray(n_dict['Cnt_Y'], dtype=np.intp))
        self.Cnt_U = garray.to_gpu(np.asarray(n_dict['Cnt_U'], dtype=np.intp))

        cuda.memcpy_htod(int(self.V), np.asarray(n_dict['V'], 
                         dtype=np.double))

        self.update = self.get_euler_kernel()
        if self.debug:
            if self.LPU_id is None:
                self.LPU_id = "Narx"
            self.I_file = tables.openFile(self.LPU_id + "_I.h5", mode="w")
            self.I_file.createEArray("/","array",
                                     tables.Float64Atom(), (0,self.num_neurons))
            self.Cnt_Y_file = tables.openFile(self.LPU_id + "_Cnt_Y.h5", mode="w")
            self.Cnt_Y_file.createEArray("/","array",
                                     tables.Float64Atom(), (0,self.num_neurons))
            self.Cnt_U_file = tables.openFile(self.LPU_id + "_Cnt_U.h5", mode="w")
            self.Cnt_U_file.createEArray("/","array",
                                     tables.Float64Atom(), (0,self.num_neurons))  
            self.Y_1_file = tables.openFile(self.LPU_id + "_Y_1.h5", mode="w")
            self.Y_1_file.createEArray("/","array",
                                     tables.Float64Atom(), (0,self.num_neurons))
            self.Y_2_file = tables.openFile(self.LPU_id + "_Y_2.h5", mode="w")
            self.Y_2_file.createEArray("/","array",
                                     tables.Float64Atom(), (0,self.num_neurons))
            self.Y_3_file = tables.openFile(self.LPU_id + "_Y_3.h5", mode="w")
            self.Y_3_file.createEArray("/","array",
                                     tables.Float64Atom(), (0,self.num_neurons))
            self.Y_4_file = tables.openFile(self.LPU_id + "_Y_4.h5", mode="w")
            self.Y_4_file.createEArray("/","array",
                                     tables.Float64Atom(), (0,self.num_neurons))
            self.Y_5_file = tables.openFile(self.LPU_id + "_Y_5.h5", mode="w")
            self.Y_5_file.createEArray("/","array",
                                     tables.Float64Atom(), (0,self.num_neurons))
            self.Y_6_file = tables.openFile(self.LPU_id + "_Y_6.h5", mode="w")
            self.Y_6_file.createEArray("/","array",
                                     tables.Float64Atom(), (0,self.num_neurons))
            self.U_1_file = tables.openFile(self.LPU_id + "_U_1.h5", mode="w")
            self.U_1_file.createEArray("/","array",
                                     tables.Float64Atom(), (0,self.num_neurons))
            self.U_2_file = tables.openFile(self.LPU_id + "_U_2.h5", mode="w")
            self.U_2_file.createEArray("/","array",
                                     tables.Float64Atom(), (0,self.num_neurons))
            self.U_3_file = tables.openFile(self.LPU_id + "_U_3.h5", mode="w")
            self.U_3_file.createEArray("/","array",
                                     tables.Float64Atom(), (0,self.num_neurons))
            self.U_4_file = tables.openFile(self.LPU_id + "_U_4.h5", mode="w")
            self.U_4_file.createEArray("/","array",
                                     tables.Float64Atom(), (0,self.num_neurons))
            self.U_5_file = tables.openFile(self.LPU_id + "_U_5.h5", mode="w")
            self.U_5_file.createEArray("/","array",
                                     tables.Float64Atom(), (0,self.num_neurons))
            self.U_6_file = tables.openFile(self.LPU_id + "_U_6.h5", mode="w")
            self.U_6_file.createEArray("/","array",
                                     tables.Float64Atom(), (0,self.num_neurons))
            self.U_7_file = tables.openFile(self.LPU_id + "_U_7.h5", mode="w")
            self.U_7_file.createEArray("/","array",
                                     tables.Float64Atom(), (0,self.num_neurons))



    @property
    def neuron_class(self): return True

    def eval(self, st=None):
        self.update.prepared_async_call(
            self.update_grid, self.update_block, st, 
            self.V,self.num_neurons, self.I.gpudata, self.dt, self.steps,                     ## Strange ddt terms again. Do we hardcode a dt to 1/400?
            self.Y_1.gpudata, self.Y_2.gpudata, self.Y_3.gpudata,  
            self.Y_4.gpudata, self.Y_5.gpudata, self.Y_6.gpudata, 
            self.U_1.gpudata, self.U_2.gpudata, self.U_3.gpudata,  
            self.U_4.gpudata, self.U_5.gpudata, self.U_6.gpudata,self.U_7.gpudata, self.Cnt_Y.gpudata, self.Cnt_U.gpudata)
        if self.debug:
            self.I_file.root.array.append(self.I.get().reshape((1, -1)))
            self.Cnt_Y_file.root.array.append(self.Cnt_Y.get().reshape((1, -1)))
            self.Cnt_U_file.root.array.append(self.Cnt_U.get().reshape((1, -1)))
            self.Y_1_file.root.array.append(self.Y_1.get().reshape((1, -1)))
            self.Y_2_file.root.array.append(self.Y_2.get().reshape((1, -1)))
            self.Y_3_file.root.array.append(self.Y_3.get().reshape((1, -1)))
            self.Y_4_file.root.array.append(self.Y_4.get().reshape((1, -1)))
            self.Y_5_file.root.array.append(self.Y_5.get().reshape((1, -1)))
            self.Y_6_file.root.array.append(self.Y_6.get().reshape((1, -1)))
            self.U_1_file.root.array.append(self.U_1.get().reshape((1, -1)))
            self.U_2_file.root.array.append(self.U_2.get().reshape((1, -1)))
            self.U_3_file.root.array.append(self.U_3.get().reshape((1, -1)))
            self.U_4_file.root.array.append(self.U_4.get().reshape((1, -1)))
            self.U_5_file.root.array.append(self.U_5.get().reshape((1, -1)))
            self.U_6_file.root.array.append(self.U_6.get().reshape((1, -1)))
            self.U_7_file.root.array.append(self.U_7.get().reshape((1, -1)))


    def get_euler_kernel(self):
        """
            ptxas info    : 0 bytes gmem
            ptxas info    : Compiling entry function 'narx_pr' for 'sm_30'
            ptxas info    : Function properties for narx_pr
                            208 bytes stack frame, 0 bytes spill stores, 0 bytes spill loads
            ptxas info    : Used 56 registers, 480 bytes cmem[0], 120 bytes cmem[2]


        """
        template = """

    #define NNEU %(nneu)d //NROW * NCOL
    #define Y_max 6   // Lag of 6
    #define U_max 7   // Lag of 7

    __global__ void

    narx_pr(%(type)s* V, int num_neurons, 
            %(type)s* I_pre, %(type)s dt, int nsteps,
            %(type)s* Y_1, %(type)s* Y_2, %(type)s* Y_3, 
            %(type)s* Y_4, %(type)s* Y_5, %(type)s* Y_6, 
            %(type)s* U_1, %(type)s* U_2, %(type)s* U_3, 
            %(type)s* U_4, %(type)s* U_5, %(type)s* U_6, %(type)s* U_7, 
            int* Cnt_Y, int* Cnt_U)   
    {
        int bid = blockIdx.x;
        int nid = (bid * NNEU) + threadIdx.x;
        
        %(type)s v,I;
        int  cnt_Y,cnt_U,ku,ky,a;

        if(nid < num_neurons)
        {   

            
            
            %(type)s y[12] ={Y_1[nid],Y_2[nid],Y_3[nid],Y_4[nid],Y_5[nid],Y_6[nid],0,0,0,0,0,0};
            %(type)s u[14] ={U_1[nid],U_2[nid],U_3[nid],U_4[nid],U_5[nid],U_6[nid],U_7[nid],0,0,0,0,0,0,0};
        
            for( a = 0; a < Y_max; a = a + 1 ){y[a+Y_max] = y[a];}
            for( a = 0; a < U_max; a = a + 1 ){u[a+U_max] = u[a];}



            %(type)s theta[15] ={ 8.66998468e-01,
                                  6.75079413e-02,
                                 -9.12842610e+01,
                                  2.48898884e-02,
                                  8.17481987e+00,
                                 -8.41121569e-01,
                                 -1.29035677e+00,
                                 -7.79721854e+01,
                                 -9.84690687e-02,
                                  2.35255885e-02,
                                  1.74602660e+00,
                                 -4.31724964e+00,
                                  2.22325254e+02,
                                  1.59757439e+01,
                                  5.59346962e+01 };

            I     = I_pre[nid];
            cnt_Y = Cnt_Y[nid];
            cnt_U = Cnt_U[nid];
             
            ku = cnt_U + U_max;
            ky = cnt_Y + Y_max;           

            
            v=    theta[0] *y[ky-1]+        
                  theta[1] *y[ky-3]+        
                  theta[2] *u[ku-5]*u[ku-4]+
                  theta[3]                 +
                  theta[4] *u[ku-6]        +
                  theta[5] *u[ku-4]*y[ky-6]+
                  theta[6] *u[ku-7]        +
                  theta[7] *u[ku-7]*u[ku-6]+
                  theta[8] *y[ky-4]        +
                  theta[9] *y[ky-5]        +
                  theta[10]*u[ku-4]*y[ky-5]+
                  theta[11]*u[ku-4]*y[ky-2]+
                  theta[12]*u[ku-7]*u[ku-3]+
                  theta[13]*u[ku-5]        +
                  theta[14]*u[ku-4];

            V[nid] =v ;
            
            // Update buffers according to counter position            
            switch(cnt_Y) { case 0 : Y_1[nid] = v; break; 
                            case 1 : Y_2[nid] = v; break; 
                            case 2 : Y_3[nid] = v; break; 
                            case 3 : Y_4[nid] = v; break; 
                            case 4 : Y_5[nid] = v; break;
                            case 5 : Y_6[nid] = v; break;
                          }   
            switch(cnt_U) { case 0 : U_1[nid] = I; break; 
                            case 1 : U_2[nid] = I; break; 
                            case 2 : U_3[nid] = I; break; 
                            case 3 : U_4[nid] = I; break; 
                            case 4 : U_5[nid] = I; break; 
                            case 5 : U_6[nid] = I; break;
                            case 6 : U_7[nid] = I; break;
                          }


            //Increment Y and U counters
            cnt_Y++;
            cnt_U++;
            
            if(cnt_Y >= Y_max){cnt_Y = 0;}
            if(cnt_U >= U_max){cnt_U = 0;}
            
            Cnt_Y[nid] = cnt_Y;
            Cnt_U[nid] = cnt_U;            
        }

    }
    """ 
        dtype = np.double
        scalartype = dtype.type if dtype.__class__ is np.dtype else dtype
        self.update_block = (128, 1, 1)
        self.update_grid = ((self.num_neurons - 1) / 128 + 1, 1)
        mod = SourceModule(template % {"type": dtype_to_ctype(dtype),
                           "nneu": self.update_block[0]}, 
                           options=["--ptxas-options=-v"])
        func = mod.get_function("narx_pr")

        func.prepare([  np.intp,        # V 
                        np.int32,       # int num_neurons
                        np.intp,        # I_Pre
                        np.intp,        # dt
                        np.int32,      # int n_steps
                        np.intp,        # Y1
                        np.intp,        # Y2
                        np.intp,        # Y3
                        np.intp,        # Y4
                        np.intp,        # Y5
                        np.intp,        # Y6
                        np.intp,        # U1
                        np.intp,        # U2
                        np.intp,        # U3
                        np.intp,        # U4
                        np.intp,        # U5
                        np.intp,        # U6
                        np.intp,        # U7
                        np.intp,        # CNT_Y
                        np.intp])       # CNT_U       


        return func
