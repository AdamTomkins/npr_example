#!/usr/bin/env python

import setuptools
from distutils.core import setup

setup(name='neurokernel-npr-example',
      version='1.0',
      packages=['npr_example',
                'npr_example.LPU',
                'npr_example.models',
                'npr_example.neurons',
                'npr_example.synapses',
		        'npr_example.utils'],
      install_requires=[
        'configobj >= 5.0.0',
        'neurokernel >= 0.1'
      ]
     )
